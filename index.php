<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>24h Challenge</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/bootstrap.css">
  <link rel="stylesheet" href="/css/style.css">
 
</head>
<body>   
    <section id="brand-bg"> 
        <img style="z-index:1;position:realtive;" id="logo" src="img/logo2.png" alt="">
        <!--<h1 style="z-index:1;position:realtive;">FooBar Digital Agency</h1>-->
    </section>
    <section class="we-are">
        <div class="row">
            <span class="line-top">QUALITY</span></br>
            <span class="line-mid">WEB DESIGN &</span></br>
            <span class="line-end">DEVELOPMENT</span></br>
            <hr>
            <p class="copy">We are out of the box thinkers with a focus on web production. 
                We'll use the latest tools to build your Brand's online presence. </p>
        </div>    
    </section>
    <section class="we-do" id="particles-js">
        <div style="z-index:1;position:realtive;" class="row">
            <h1 id="title">What We Do</h1>
            <hr class="we-do-hr"></br>
            <span class="line-top">Planing</span></br>
            <span class="line-top">Design</span></br>
            <span class="line-top">Development</span></br>
            <span class="line-top">Optimization</span></br>
            <span class="line-top">UI/UX</span></br>
            <span class="line-top">Aplications</span></br>
            <span class="line-top">CMS/CRM</span></br>
            <span class="line-top">Mobile</span></br>
            <span class="line-top">Analytics</span></br>
            <span class="line-top">Social Media</span></br>
            <span class="line-top">Consulting</span></br>
        </div>  
    </section>
    <section class="blog">
        <span id="blog-title">From the Blog</span></br>
        <div class="article">
            <span id="meta">TUTORIALS - 10.10.2016</span>
            <h1 id="article-title"><a href="#">How to - Social Media.</a></h1>
        </div>
        <div class="article">
            <span id="meta">CASUAL - 05.10.2016</span>
            <h1 id="article-title"><a href="#">Something, something about Web Design.</a></h1>
        </div>
        <div class="article">
            <span id="meta">VENTING - 01.10.2016</span>
            <h1 id="article-title"><a href="#">Why the javascript comunity is insane!</a></h1>
        </div>
    </section>
    <section class="contact">
        <div class="row">
            <div class="col-md-6">
                <div id="contact-copy">
                    <h3>Get in touch</h3>
                    <h4>With us</h4>
                    <p class="copy-hero">And tell us what we can do for you!</p>
                </div>
            </div>
            <div class="col-md-6">
                <form action="#">
                    <input id="name" type="text" class="form-control" placeholder="Name">
                    <input id="email" type="text" class="form-control" placeholder="Email">
                    <textarea name="message" id="messages" placeholder="Your message" ></textarea>
                    <input type="submit" name="submit" class="submit" value="Submit">
                </form>
            </div>
        </div>
    </section>
    <section class="footer">
            <div class="col-md-12">
                <span style="margin-top:20px;">&copy; 2016 - Foobar Digital</span>
                <div class="social-icons pull-right">
                    <ul class="nav nav-pills">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div> 
            </div>       
    </section>

</script>
<!-- js stuff -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
<script src="js/efx/particles.js"></script>
<script src="js/efx/app.js"></script>

</body>
</html>